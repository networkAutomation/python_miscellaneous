#!/usr/bin/env python3
import os, json, re, threading
from multiprocessing import Process
from urllib import request, parse

def main():
    time = re.compile(r'(time=)(\d+\.\d+)')
    with open(os.path.expanduser('~/tdb3.tmol.ams0.websys.tmcs-p10-226-8-68-IE.txt'), 'r') as log:
        while True:
            for i in log.readlines():
                if ' S ' not in i and 'SA' not in i and 'time=' not in i:
                    if 'RCVD' in i:
                        sendMessageToSlack(i)
                if 'time=' in i:
                    rtt = time.search(i)
                    if float(rtt.group(2)) > 20:
                        sendMessageToSlack(i)
                    elif 'Request timeout' in i:
                        sendMessageToSlack(i)

def alive():
    threading.Timer(300.0, alive).start()
    sendMessageToSlack('I am still alive [E. Vedder]')

def sendMessageToSlack(text):
    post = {"text": "{0}".format(text)}
    try:
        json_data = json.dumps(post)
        req = request.Request("https://hooks.slack.com/services/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", data=json_data.encode('ascii'), headers={'Content-Type': 'application/json'})
        resp = request.urlopen(req)
    except Exception as em:
        print("EXCEPTION: " + str(em))

if __name__ == '__main__':
    Process(target=alive()).start()
    Process(target=main()).start()
