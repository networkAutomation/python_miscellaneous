#!/usr/bin/env python3

def myLogin(ip, username, password):
    myDict = {}
    myDict['ip'] = ip
    myDict['username'] = username
    myDict['password'] = password
    return myDict

R1 = myLogin('192.168.1.1','olivierif','supersecretpassword')
print(R1)
SW1 = myLogin('10.0.0.2','olivierif','supersecure')
print(SW1)
############################################################
class networkDevice(object):
    pass

R1 = networkDevice()
print(R1)

R1.ip = '192.168.1.1'
R1.username = 'olivierif'
R1.password = 'supersecret'

print('My ip si {}, My username is {}, My password is {}'.format(R1.ip,R1.username,R1.password))

class newNetworkDevice(object):
    def __init__(self,ip,username,password):
        self.ip = ip
        self.username = username
        self.password = password

# (object): no inheritance from other classes
# def __init__: init method automatically executed when the object is created
# (self,ip,..): self is a reference to the object (R1 in this case)
# self.ip: is an attribute of the class
# R1 variable creates an object in memory for newNetworkDevice class

R1 = newNetworkDevice('192.168.1.1','olivierif','supersecret')
print(R1.ip,R1.username,R1.password)
############################################################
class Employee(object):
    'common base clase for all employee'
    empCount = 0

    def __init__(self, name, surname, pay):
        self.name = name
        self.surname = surname
        self.pay = pay
        Employee.empCount += 1

    def displayCount(self):
        print('Total employee {}'.format(Employee.empCount))

    def displayEmployee(self):
        print('Name: ', self.name, ', Surname: ', self.surname, ', Pay: ', self.pay)

    def summPay(self):
        pass

emp1 = Employee('Federico', 'Olivieri', '5000')
#print(emp1)
print(emp1.name, emp1.surname, emp1.pay)
emp1.displayCount()
emp1.displayEmployee()

emp2 = Employee('Giulia', 'Da San Martino', '6500')
emp2.displayCount()
totEmp = print('We have {} in our company'.format(Employee.empCount))

emp1.age = 31
emp2.age = 25

print('The age of our employee are {} and {}'.format(emp1.age, emp2.age))
