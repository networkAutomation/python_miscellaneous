import paramiko, socket

class myClass(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y
    def sum(self):
        print(self.x + self.y)

class customApp(object):
    appDicTCP = {
    80:'junos-http',
    443:'junos-https',
    22:'junos-ssh',
    21:'junos-ftp',
    53:'junos-dns-tcp',
    'any':'junos-tcp-any'}
    appDicUDP = {
    123:'junos-ntp',
    25:'junos-snmp',
    53:'junos-dns-udp',
    514:'junos-syslog',
    'any':'junos-udp-any' }
    def __init__(self, prot, port):
        if prot == 'udp':
            print(self.appDicUDP[port])
        elif prot == 'tcp':
            print(self.appDicTCP[port])

class networkManager(object):
    def __init__(self,ip,username,password):
        self.ip = ip
        self.username = username
        self.password = password
        self.ssh =  paramiko.SSHClient()
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.ssh.connect(ip, username=username, password=password, look_for_keys=False, allow_agent=False)
        self.connection = self.ssh.invoke_shell()
        self.connection.send('show running-config')
        return self.connection.recv(65535)
    # def showRun(self):
    #     connection = networkManager.connection
    #     self.connection.send('show running-config')
    #     return self.connection.recv(65535)
