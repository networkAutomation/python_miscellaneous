def main():
    import ruamel.yaml
    yaml = ruamel.yaml.YAML()
    yaml.indent(mapping=2, sequence=2, offset=2)

    with open('/home/federico/git/myowngitlab/networkAutomation/python/jinja/YAML/juniper/mgmt/mgmt.yaml') as yamlMgmt:
        mgmt = yaml.load(yamlMgmt)
    with open('/home/federico/git/myowngitlab/networkAutomation/python/jinja/YAML/juniper/mgmt/users.yaml') as yamlUsers:
        users = yaml.load(yamlUsers)
    for i in users['yamlFile']:
        mgmt['yamlFile'].update({i:users['yamlFile'][i]})
    yaml.dump(mgmt,open('lal.yaml', 'w'))

if __name__ == '__main__':
    main()
