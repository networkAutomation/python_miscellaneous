#!/usr/bin/env python

import re, datetime, sys

starTime = datetime.datetime.now()

term1 = re.compile('EDGE-ACL-IPv4-IN')
term2 = re.compile('packets')

fileList = sys.argv[1]

with open (fileList, 'r') as f:
    for fileList in f.readlines():
        if re.search(term1, fileList) and re.search(term2, fileList):
            print(fileList)
