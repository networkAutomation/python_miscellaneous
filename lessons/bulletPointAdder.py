#!/usr/bin/env python3
# bullettPointAdder.py - Adds bullent points to the start
# of each line of text on the clipboard.

import pyperclip
text = pyperclip.paste()

lines = text.split('\n')
for i in range(len(lines)):
    lines[i] = '* ' + lines[i]
text = '\n'.join(list)

pyperclip.copy(text)
