# Notes for Regex Python3
# from "Automate The Boring Stuff With Python - Al Sweigart "

import re

# matching regex object
phoneNumber = re.compile(r'\d\d\d-\d\d\d-\d\d\d\d')
mo = phoneNumRegex.search('My number is 415-555-4242.)
print('Phone number found: ' + mo.group())
# 'Phone number found 415-555-4242'

# grouping with parentheses. To escape parenteses \( or \)
phoneNumber = re.compile(r'(\d\d\d)-(\d\d\d)-(\d\d\d\d)')
mo = phoneNumRegex.search('My number is 415-555-4242.)
mo.group(1)
# '415'
mo.group(2)
# '555-4242'
mo.group(0)
# '415-555-4242'
mo.group()
# '415-555-4242'
mo.groups()
# ('415', '555-4242')
areaCode, mainNumber = mo.groups()
print(areaCode)
# 415
print(mainNumber)
# 555-4242

ip = '192.168.1.0/24'
compile = re.compile(r'((?:\d+\.){3}\d+)/(\d+)')
search = compile.search(ip)
print(search.group())
'192.168.1.0/24'
print(search.group(0))
'192.168.1.0/24'
print(search.group(1))
'192.168.1.0'
print(search.group(2))
'24'

it = 'federico_olivieri@myemail.it'
uk = 'federico_olivieri@myemail.co.uk'
emailRegex = re.compile(r'([a-zA-Z0-9._-]+@[a-zA-Z]+)(\.[a-zA-Z]{2})?(\.[a-zA-Z]{2,4})')
itg = emailRegex.search(it)
itg.group()
'federico_olivieri@hotmail.it'
ukg = emailRegex.search(uk)
ukg.group()
'federico_olivieri@myemail.co.uk'


# matching multipe groups with the pipe
heroRegex = re.compile(r'Batman|Tina Fey')
mo1 = heroRegex.search('Batman and Tina Fey')
mo1.group()
# 'Batman'
mo2 = heroRegex.search('Tina Fey and Batman')
mo2.group
# 'Tina Fey'
batRegex = re.compile(r'Bat(man|mobile|copter|bat)')
mo1 = batRegex.search('Batmobile lost a wheel')
mo1.group()
# 'Batmobile'
mo.group(1)
# 'Batmobile'

# optional matching with question mark
batRegex = re.compile(r'Bat(wo)?man')
mo1 = batRegex.search('The Adventures of Batman')
mo1.group()
# 'Batman'
mo2 = batRegex.search('The Adventures of Batwoman')
mo2.group()
# 'Batwoman'
phoneNumber = re.compile(r'(\d\d\d-)?\d\d\d-\d\d\d\d')
mo1 = phoneNumRegex.search('My number is 415-555-4242.')
mo1.group()
# '415-555-4242'
mo2 = phoneNumRegex.search('My number is 555-4242.')
mo2.group()
# '555-4242'

# matching zero or more with the star
batRegex = re.compile(r'Bat(wo)*man')
mo1 = batRegex.search('The Adventures of Batman')
mo1.group()
# 'Batman'
mo2 = batRegex.search('The Adventures of Batwoman')
mo2.group()
# 'Batwoman'
mo3 = batRegex.search('The Adventures of Batwowowoman')
mo3.group()
# 'Batwowowoman'

# matching at least one or more with the plus
batRegex = re.compile(r'Bat(wo)+man')
mo1 = batRegex.search('The Adventures of Batwoman')
mo1.group()
# 'Batwoman'
mo2 = batRegex.search('The Adventures of Batwowowoman')
mo2.group()
# 'Batwowowoma
mo3 = batRegex.search('The Adventures of Batman')
mo3 == None
# True

# matching specific repetitions with curly brackets
haRegex = re.compile('r(Ha){3}')
mo1 = haRegex.search('HaHaHa')
mo1.group()
# 'HaHaHa'
mo2 = haRegex.search('Ha')
mo2 == None
# True

#greedy and nongreedy
greedyHaRegex = re.compile('r(Ha){3,5}')
mo1 = greedyHaRegex.search('HaHaHaHaHa')
mo1.group()
# HaHaHaHaHa
nongreedyHaRegex = re.compile('r(Ha){3,5}?')
mo2 = nongreedyHaRegex.search('HaHaHaHaHa')
mo2.group()
# HaHaHa

# the findall() method
# search() returns the first match in string
# findall() returns the string of every match
# look for difference between expression with groups and
# expression without groups
phoneNumber = re.compile(r'\d\d\d-\d\d\d-\d\d\d\d') # has no groups
mo1 = phoneNumRegex.search('Work: 415-555-4242 Cell: 415-345-6789')
mo1.group()
# '415-555-4242'
mo2 = phoneNumRegex.findall('Work: 415-555-4242 Cell: 415-345-6789')
mo2.group()
# ['415-555-4242', '415-345-6789'] # return a list
phoneNumber = re.compile(r'(\d\d\d)-(\d\d\d)-(\d\d\d\d)') # has groups
mo3 = phoneNumRegex.findall('Work: 415-555-4242 Cell: 415-345-6789')
mo3.group()
# [('415', '555', '4242', '415' '345' '6789'] # return a tuple

# character classes
# \d --> Any numerci digit from 0 to 9
# \D --> Any character that is NOT a numeric digit from 0 to 9
# \w --> Any letter, numeric digit, or underscore character
# \W --> Any character that is NOT a letter, numeric digit or the underscore character
# \s --> Any space, tab or newline character
# \S --> Any character that is NOT a space, tab, or newline

# Making Your Own Character Classes
vowelRegex = re.compile(r'[aeiouAEIOU]')
vowelRegex.findall('RoboCop eats baby food. BABY FOOD.')
# ['o', 'o', 'o', 'e', 'a', 'a', 'o', 'o', 'A', 'O', 'O']

# Making Your Own Character Classes - Negative
vowelRegex = re.compile(r'[^aeiouAEIOU]')
vowelRegex.findall('RoboCop eats baby food. BABY FOOD.')
# ['R', 'b', 'C', 'p', ' ', 't', 's', ' ', 'b', 'b', 'y', ' ', 'f', 'd', '.', ' ', 'B', 'B', 'Y', ' ', 'F', 'D', '.']

# The Caret and Dollar Sign Characters
beginsWithHello = re.compile(r'^Hello')
beginsWithHello.search('Hello World')
# <_sre.SRE_Match object at 0x7f16d4b30578>
beginsWithHello.search('He SaidHello World') == True
# False

ensdWithNumber = re.compile(r'\d$')
ensdWithNumber.search('Your number is 42')
# <_sre.SRE_Match object; span=(16, 17), match='2'>
ensdWithNumber.search('Your number is forty two.') == None
# True

wholeStringIsNum = re.compile(r'^\d+$')
wholeStringIsNum.search('1234567890')
# <_sre.SRE_Match object; span=(0, 10), match='1234567890'>
wholeStringIsNum.search('123456xyx7890') == None
# True
wholeStringIsNum.search('123456   7890') == None
# True

# The Wildcard Character
atRegex = re.compile(r'.at')
atRegex.findall('The cat in the at sat on the flat mat.')
# ['cat', ' at', 'sat', 'lat', 'mat']

# Match everything with Dot-Star
nameRegex = re.compile(r'First Name: (.*) Last Name: (.*)')
mo = nameRegex.search('First Name: Al Last Name: Sweigart')
mo.group(1)
# 'Al'
mo.group(2)
# 'Sweigart'

# Match everything with Dot-Star - greedy/nongreedy
nongreedyRegex = re.compile(r'<.*?>')
mo = nongreedyRegex.search('<To serve man> for dinner.>')
mo.group()
# <To serve man>
mo.group(2)
# 'Sweigart'
