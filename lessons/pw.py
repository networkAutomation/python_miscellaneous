#!/usr/bin/env python
# pw.py - An insecure password locker program

import sys, pyperclip

account = sys.argv[1]
PASSWORD = { 'email': 'F7yh5Rsn237Kms', 'blog': '123Abc567DEf', 'luggage': '0779' }

if len (sys.argv) < 2:
    print ('Usage: python3 pw.py [account] - copy account password')
    sys.exit()

if account in PASSWORD:
    pyperclip.copy(PASSWORD[account]) #PASSWORD[acount} - square brackets to access to dictionary's key
    print ('Password for ' + account + ' copied to clipboard.')
else:
    print('There is no account named ' + account)
