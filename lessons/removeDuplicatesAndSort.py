#!/usr/bin/env python3
# remove duplicates in a list with "set"
# set: an unordered collections of distinct objects
values = ['1','2','3','4','2','2','3']

def distinctSorted(ott):
    print(sorted(list(set(ott))))

distinctSorted(values)
# output example
# ['1', '2', '3', '4']
