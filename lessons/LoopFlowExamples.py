#!/usr/bin/env python3

def whileLoop(i):
    while i <= 10:
        print(i)
        i += 1

whileLoop(1)
############################################################
def forLoop(a):
    for i in a:
        print(i)

forLoop(range(1,9))
############################################################
def ifLoop(i):
    while i <= 10:
        if i == 5:
            print('i is equal to {}'.format(i))
            i += 1
        elif i == 9:
            print('this is "almost" the end')
            i += 1
        else:
            print('this is {}'.format(i))
            i += 1

ifLoop(1)
############################################################
myDict = {'ip':'192.168.1.1', 'username':'olivierif', 'password':'SuperSecret'}
for key,val in myDict.items():
    print('My {} is {}'.format(key,val))
