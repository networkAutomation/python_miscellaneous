#! /usr/bin/env python3
def show(command):
    print("Sending 'show' command...")
    print('Command sent: ', command)
def config(command):
    print("Sending 'config' command...")
    print('Commands sent: ', command)
if __name__ == "__main__":
    command = 'show version'
    show(command)
    command = 'interface Eth1/1 ; shutdown'
    config(command)
