#!/usr/bin/env python3

import yaml,sys,getpass
from netmiko import ConnectHandler

def main():

    finalConf = []

    inventory = yaml.load(open(sys.argv[1], 'rb'))

    print('\n' + '#' * 20 + ' THIS IS MY PARSED YAML FILE ' + '#' * 20 + '\n')
    print(inventory)
    print('\n' + '#' * 20 + ' END ' + '#' * 20 + '\n')

    for interf in inventory['interfaces']:
#        print(interf)
        if interf['int']['name'].startswith('Gi'):
#            print('int {}'.format(interf['int']['name']))
            finalConf.append('int {}'.format(interf['int']['name']))
#            print('ip address {} {}'.format(interf['int']['ipv4'], interf['int']['mask']))
            finalConf.append('ip address {} {}'.format(interf['int']['ipv4'], interf['int']['mask']))
            if interf['int']['enabled'] == True:
#                print('no shut')
                finalConf.append('no shut')
            elif interf['int']['enabled'] == False:
#                print('shut')
                finalConf.append('shut')
            else:
                print('Please specify False or True')

            for ipv6 in interf['int']['ipv6']:
#                print('ipv6 enabled')
                finalConf.append('ipv6 enabled')
#                print('ipv6 address {}'.format(ipv6))
                finalConf.append('ipv6 address {}'.format(ipv6))

    print('\n' + '#' * 20 + ' THESE ARE MY CONFIG LINES ' + '#' * 20 + '\n')
    print(finalConf)
    print('\n' + '#' * 20 + ' END ' + '#' * 20 + '\n')

    username = input('Username: ')
    password = getpass.getpass()
    sshSession = ConnectHandler(device_type='cisco_ios', ip='ios.aut.lab', username=username,password=password, verbose=True)
    sshSession.send_config_set(finalConf)
    print(sshSession.send_command('sh ip int brief'))
    ipv6Check = (sshSession.send_command('sh ipv6 int brief'))
    print(ipv6Check)
    if '2003::3' in ipv6Check:
        print('Gi0/2 has got an ipv6')
    else:
        print('Gi0/2 is ipv4 only')

if __name__ == '__main__':
    main()
