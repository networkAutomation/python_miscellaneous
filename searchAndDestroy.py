#!/usr/bin/env python3
# find a word in a text file and delete it

def searchAndDestroy():
  print('''Please, rename the source file in "inputFile.txt" and save it in local folder where script is running.
Output file will be saved on local folder and renamed outputFile.txt''')
  find = input('Enter the word you want delete: ')
  with open('inputFile.txt') as inputFile, open('outputFile.txt', 'w') as outputFile:
    for line in inputFile:
        if line.rstrip() != find:
          outputFile.write(line)
 
searchAndDestroy()
