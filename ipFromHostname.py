#!/usr/bin/env python3
import socket

def ipFromHost():
  """resolve hostname to an ip, from user-input or file"""
  opt1 = input('Do you want provide  hostname or file: ') 
  while opt1 not in ['hostname', 'file']:
    opt1 = input('Please, type "hostname" or "file": ')
  if opt1 == 'hostname':
    optHostname = input('Please, provide hostname: ')
    print(socket.gethostbyname(optHostname.strip()))
  elif opt1 == 'file':
    optFile = input('Please, provide file name: ')
    with open(optFile) as inputFile:
      for i in inputFile:
        print(socket.gethostbyname(i.strip()))
    
ipFromHost()
